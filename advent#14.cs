using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{

    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            Dictionary<double, double> values = new Dictionary<double, double>();

            Regex maskRegex = new Regex("mask = ([01X]+)");
            Regex memRegex = new Regex(@"mem\[(\d+)\] = (\d+)");

            Mask currentMask = new Mask();
            foreach (var line in lines)
            {
                if (maskRegex.IsMatch(line))
                {
                    currentMask = new Mask(maskRegex.Match(line));
                }
                else
                {
                    var match = memRegex.Match(line);
                    var address = int.Parse(match.Groups[1].Value);
                    var number = int.Parse(match.Groups[2].Value);

                    values[address] = currentMask.GetValueMasked(number);
                }
            }

            var sum = values.Sum(k => k.Value);

            values = new Dictionary<double, double>();
            foreach (var line in lines)
            {
                if (maskRegex.IsMatch(line))
                {
                    currentMask = new Mask(maskRegex.Match(line));
                }
                else
                {
                    var match = memRegex.Match(line);
                    var addressNumber = int.Parse(match.Groups[1].Value);
                    var number = int.Parse(match.Groups[2].Value);

                    var addresses = currentMask.GetAddresses(addressNumber);
                    foreach (var address in addresses)
                    {
                        values[address] = number;
                    }
                }
            }

            sum = values.Sum(k => k.Value);

            Console.Read();
        }
    }

    class Mask
    {

        private char[] _bitMask;

        public Mask(){}

        public Mask(Match match)
        {
            _bitMask = match.Groups[1].Value.ToCharArray();
        }

        public double GetValueMasked(int decimalNumber)
        {
            string binary = Convert.ToString(decimalNumber, 2);
            var binaryArray = binary.PadLeft(36, '0').ToCharArray();


            for (int i = 0; i < _bitMask.Length; i++)
            {
                if (_bitMask[i] != 'X')
                {
                    binaryArray[i] = _bitMask[i];
                }
            }

            binary = new string(binaryArray);
            var value  = Convert.ToInt64(binary.TrimStart(new[] {'0'}), 2);

            return value;
        }

        public List<double> GetAddresses(int decimalNumber)
        {
            string binary = Convert.ToString(decimalNumber, 2);
            var binaryArray = binary.PadLeft(36, '0').ToCharArray();


            for (int i = 0; i < _bitMask.Length; i++)
            {
                if (_bitMask[i] == '0') continue;
                if (_bitMask[i] == '1') binaryArray[i] = '1';
                if (_bitMask[i] == 'X') binaryArray[i] = 'X';
            }

            return GetPermutations(binaryArray);
        }

        public List<double> GetPermutations(char[] binaryArray)
        {
            if (binaryArray.All(c => c != 'X'))
            {
                var binary = new string(binaryArray);
                return new List<double>{Convert.ToInt64(binary.TrimStart(new[] { '0' }), 2)};
            }

            List<double> values = new List<double>();
            for (int i = 0; i < binaryArray.Length; i++)
            {
                if (binaryArray[i] == 'X')
                {
                    var subArray = new char[36];
                    Array.Copy(binaryArray, 0, subArray, 0, subArray.Length);
                    subArray[i] = '0';
                    values = values.Concat(GetPermutations(subArray)).ToList();
                    subArray[i] = '1';
                    values = values.Concat(GetPermutations(subArray)).ToList();
                    break;
                }
            }

            return values;
        }
    }
}
