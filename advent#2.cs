
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            List<PasswordInfos> passwordInfos = new List<PasswordInfos>();

            Regex rgx = new Regex("(\\d+)-(\\d+)\\s([a-z]): ([a-z]+)");
            foreach (string line in lines)
            {
                var match = rgx.Match(line);
                var min = int.Parse(match.Groups[1].Value);
                var max = int.Parse(match.Groups[2].Value);
                var password = match.Groups[4].Value;
                var letter = match.Groups[3].Value;

                passwordInfos.Add(new PasswordInfos(min, max, password, letter));
            }

            int count = 0;
            foreach (var pw in passwordInfos)
            {
                if (pw.IsOk1()) count++;
            }
            Console.WriteLine(count);

            count = 0;
            foreach (var pw in passwordInfos)
            {
                if (pw.IsOk2()) count++;
            }
            Console.WriteLine(count);
            Console.ReadLine();
        }
    }

    class PasswordInfos
    {
        public PasswordInfos(int min, int max, string password, string letter)
        {
            Min = min;
            Max = max;
            Password = password;
            Letter = letter.ToCharArray()[0];
        }

        public int Min { get; set; }
        public int Max { get; set; }
        public string Password { get; set; }
        public char Letter { get; set; }

        public bool IsOk1()
        {
            var count = Password.Count(x => x == Letter);
            return count >= Min && count <= Max;
        }
        public bool IsOk2()
        {
            var firstRespected = Password.ToCharArray()[Min - 1] == Letter;
            var secondRespected = Password.Length >= Max && Password.ToCharArray()[Max -1] == Letter;
            return firstRespected ^ secondRespected;
        }
    }
}
