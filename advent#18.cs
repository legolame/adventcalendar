using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{

    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            var parenthesisRegex = new Regex(@"\(([\d\+\*]+)\)");
            var results = new List<ulong>();

            foreach (var line in lines)
            {
                var equation = RemoveWhitespace(line);
                while (parenthesisRegex.IsMatch(equation))
                {
                    var matches = parenthesisRegex.Matches(equation);
                    foreach (Match match in matches)
                    {
                        var result = GetResultFromExpression(match.Groups[1].Value);
                        equation = equation.Replace(match.Value, result);
                    }
                }

                results.Add(ulong.Parse(GetResultFromExpression(equation)));
            }

            ulong sum = 0;
            foreach (var result in results)
            {
                sum += result;
            }

            Console.WriteLine(sum);

            Console.Read();
        }

        static string GetResultFromExpression(string expression)
        {
            var expressionRegex = new Regex(@"(\d+)(\+)(\d+)");
            while (expressionRegex.IsMatch(expression))
            {
                var match = expressionRegex.Match(expression);
                var result = GetResultFromOperation(ulong.Parse(match.Groups[1].Value), ulong.Parse(match.Groups[3].Value),
                    match.Groups[2].Value);

                expression = expressionRegex.Replace(expression, result, 1);
            }

            expressionRegex = new Regex(@"(\d+)(\*)(\d+)");
            while (expressionRegex.IsMatch(expression))
            {
                var match = expressionRegex.Match(expression);
                var result = GetResultFromOperation(ulong.Parse(match.Groups[1].Value), ulong.Parse(match.Groups[3].Value),
                    match.Groups[2].Value);

                expression = expressionRegex.Replace(expression, result, 1);
            }

            return expression;
        }
        
        static string GetResultFromOperation(ulong firstNumber, ulong secondNumber, string symbol)
        {
            if (symbol == "+")
                return (firstNumber + secondNumber).ToString();
            
            return (firstNumber * secondNumber).ToString();
        }

        static string RemoveWhitespace(string input)
        {
            return new string(input.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }
    }
}
