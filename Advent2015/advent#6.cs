using System;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    class Program
    {

        static bool[,] lights = new bool[1000, 1000];
        static int[,] dimLights = new int[1000, 1000];

        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");
            Regex regex = new Regex(@"(\d+),(\d+) through (\d+),(\d+)");
            foreach (var line in lines)
            {
                bool instructionIsToggle = line.Contains("toggle");
                bool state = line.Contains("turn on");

                var match = regex.Match(line);
                var minX = int.Parse(match.Groups[1].Value);
                var minY = int.Parse(match.Groups[2].Value);
                var maxX = int.Parse(match.Groups[3].Value);
                var maxY = int.Parse(match.Groups[4].Value);

                for (int x = minX; x <= maxX; x++)
                {
                    for (int y = minY; y <= maxY; y++)
                    {
                        if (instructionIsToggle)
                        {
                            ToggleLightState(x, y);
                        }
                        else
                        {
                            SetLightState(x, y, state);
                        }
                    }
                }
            }

            var litCounter = 0;
            for (int x = 0; x < 1000; x++)
            {
                for (int y = 0; y < 1000; y++)
                {
                    litCounter += lights[x, y] ? 1 : 0;
                }
            }

            Console.WriteLine(litCounter);

            //PartB

            foreach (var line in lines)
            {
                bool instructionIsToggle = line.Contains("toggle");
                bool state = line.Contains("turn on");

                var match = regex.Match(line);
                var minX = int.Parse(match.Groups[1].Value);
                var minY = int.Parse(match.Groups[2].Value);
                var maxX = int.Parse(match.Groups[3].Value);
                var maxY = int.Parse(match.Groups[4].Value);

                for (int x = minX; x <= maxX; x++)
                {
                    for (int y = minY; y <= maxY; y++)
                    {
                        if (instructionIsToggle)
                        {
                            AdjustLightBrightness(x, y, 2);
                        }
                        else
                        {
                            AdjustLightBrightness(x, y, state ? 1 : -1);
                        }
                    }
                }
            }

            var brightnessCounter = 0;
            for (int x = 0; x < 1000; x++)
            {
                for (int y = 0; y < 1000; y++)
                {
                    brightnessCounter += dimLights[x, y];
                }
            }

            Console.WriteLine(brightnessCounter);

            Console.Read();
        }

        static void SetLightState(int x, int y, bool state)
        {
            lights[x, y] = state;
        }

        static void ToggleLightState(int x, int y)
        {
            lights[x, y] = !lights[x, y];
        }

        static void AdjustLightBrightness(int x, int y, int amount)
        {
            dimLights[x, y] += amount;
            if (dimLights[x, y] < 0) dimLights[x, y] = 0;
        }
    }
}