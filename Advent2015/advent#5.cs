using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");


            var count = lines.Count(l => ContainsAtLeastThreeVowels(l) && ContainsAtLeastOneRepetition(l) && DoesNotContainNaughtyCombos(l));
            Console.WriteLine(count);

            count = lines.Count(l => ContainsAtLeastOneFancyRepetition(l) && ContainsOneLetterSandwhich(l));
            Console.WriteLine(count);
            Console.Read();
        }

        static bool ContainsAtLeastThreeVowels(string line)
        {
            Regex vowelRegex = new Regex("[aeiou]");
            return vowelRegex.Matches(line).Count >= 3;
        }

        static bool ContainsAtLeastOneRepetition(string line)
        {
            Regex duplicationRegex = new Regex(@"([a-z])\1");
            return duplicationRegex.IsMatch(line);
        }

        static bool DoesNotContainNaughtyCombos(string line)
        {
            return !line.Contains("ab") && !line.Contains("cd") && !line.Contains("pq") && !line.Contains("xy");
        }

        static bool ContainsAtLeastOneFancyRepetition(string line)
        {
            Regex duplicationRegex = new Regex(@"([a-z]{2})(?=.*\1)");
            return duplicationRegex.IsMatch(line);
        }

        static bool ContainsOneLetterSandwhich(string line)
        {
            Regex duplicationRegex = new Regex(@"([a-z]{1})(?=[a-z]\1)");
            return duplicationRegex.IsMatch(line);
        }
    }
}