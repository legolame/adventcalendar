using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            var bags = new Dictionary<string, Bag>();
            var mainBagRegex = new Regex(@"([a-z\s]+)\sbags contain");
            foreach (var line in lines)
            {
                var bag = new Bag();
                bag.Color = mainBagRegex.Match(line).Groups[1].Value;

                var contentBagRegex = new Regex(@"([1-9]+)\s([a-z\s]+)\sbag");
                foreach (Match match in contentBagRegex.Matches(line))
                {
                    bag.AllowedContents.Add((match.Groups[2].Value, int.Parse(match.Groups[1].Value)));
                }

                bags[bag.Color] = bag;
            }

            Console.WriteLine(bags.Count(b => b.Value.CanContainColor("shiny gold", bags)));
            Console.WriteLine(bags["shiny gold"].GetTotalContent(bags));
            Console.Read();
        }
    }

    class Bag
    {
        public string Color { get; set; }
        public List<(string color, int quantity)> AllowedContents = new List<(string color, int quantity)>();

        public bool CanContainColor(string color, Dictionary<string, Bag> bags)
        {
            return AllowedContents.Exists(c => c.color == color || bags[c.color].CanContainColor(color, bags));
        }

        public int GetTotalContent(Dictionary<string, Bag> bags)
        {
            return AllowedContents.Sum(c =>c.quantity + c.quantity * bags[c.color].GetTotalContent(bags));
        }
    }
}