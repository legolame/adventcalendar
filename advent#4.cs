using System;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            Regex regex = new Regex(@"([a-z]{3}):([#a-z0-9]+)");

            var currentCheckedPassport = new Passport();
            var validPasswordCounter = 0;

            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    validPasswordCounter += currentCheckedPassport.IsValid() ? 1 : 0;
                    currentCheckedPassport = new Passport();
                    continue;
                }

                foreach (Match match in regex.Matches(line))
                {
                    currentCheckedPassport.SetPropertyFromString(match.Groups[1].Value, match.Groups[2].Value);
                }
            }

            Console.WriteLine(validPasswordCounter);
            Console.ReadLine();
        }
    }

    class Passport
    {
        public string byr { get; set; }
        public string iyr { get; set; }
        public string eyr { get; set; }
        public string hgt { get; set; }
        public string hcl { get; set; }
        public string ecl { get; set; }
        public string pid { get; set; }
        public string cid { get; set; }

        public void SetPropertyFromString(string propertyName, string value)
        {
            var propertyInfo = GetType().GetProperty(propertyName);
            propertyInfo?.SetValue(this, value);
        }

        public bool IsValid()
        {
            return ByrIsValid() &&
                   IyrIsValid() &&
                   EyrIsValid() &&
                   HgtIsValid() &&
                   HclIsValid() &&
                   EclIsValid() &&
                   PidIsValid();
        }

        private bool ByrIsValid()
        {
            if (!string.IsNullOrEmpty(byr) && int.TryParse(byr, out var numericYear))
            {
                return numericYear >= 1920 && numericYear <= 2002;
            }

            return false;
        }

        private bool IyrIsValid()
        {
            if (!string.IsNullOrEmpty(iyr) && int.TryParse(iyr, out var numericYear))
            {
                return numericYear >= 2010 && numericYear <= 2020;
            }

            return false;
        }

        private bool EyrIsValid()
        {
            if (!string.IsNullOrEmpty(eyr) && int.TryParse(eyr, out var numericYear))
            {
                return numericYear >= 2020 && numericYear <= 2030;
            }

            return false;
        }

        private bool HgtIsValid()
        {
            if (!string.IsNullOrEmpty(hgt) && hgt.Length > 2)
            {
                var unit = hgt.Substring(hgt.Length - 2);
                var value = hgt.Substring(0, hgt.Length - 2);

                if (!int.TryParse(value, out var heightNumeric))
                {
                    return false;
                }

                if (unit == "cm")
                {
                    return heightNumeric >= 150 && heightNumeric <= 193;
                }

                if (unit == "in")
                {
                    return heightNumeric >= 59 && heightNumeric <= 76;
                }

                return false;
            }

            return false;
        }

        private bool HclIsValid()
        {
            if (!string.IsNullOrEmpty(hcl) && hcl.Length == 7)
            {
                Regex regex = new Regex("#[a-f0-9]{6}");
                return regex.IsMatch(hcl);
            }

            return false;
        }

        private bool EclIsValid()
        {
            return ecl == "amb" || ecl == "blu" || ecl == "brn" || ecl == "gry" || ecl == "grn" || ecl == "hzl" ||
                   ecl == "oth";
        }

        private bool PidIsValid()
        {
            return !string.IsNullOrEmpty(pid) && pid.Length == 9 && int.TryParse(pid, out _);
        }
    }
}