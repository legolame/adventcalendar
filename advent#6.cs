using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            var currentGroup = new Group();
            List<Group> groups = new List<Group> {currentGroup};

            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    currentGroup = new Group();
                    groups.Add(currentGroup);
                    continue;
                }

                currentGroup.PeopleInGroup++;

                foreach (var question in line)
                {
                    currentGroup.QuestionAnswered[question] = currentGroup.QuestionAnswered.ContainsKey(question)
                        ? currentGroup.QuestionAnswered[question] + 1
                        : 1;
                }
            }

            Console.WriteLine(groups.Sum(g => g.GetNumberOfDifferentQuestions()));
            Console.WriteLine(groups.Sum(g => g.GetNumberOfQuestionsEverybodyAnswered()));
            Console.Read();
        }
    }

    class Group
    {
        public Dictionary<char, int> QuestionAnswered = new Dictionary<char, int>();
        public int PeopleInGroup;

        public int GetNumberOfDifferentQuestions()
        {
            return QuestionAnswered.Count;
        }

        public int GetNumberOfQuestionsEverybodyAnswered()
        {
            return QuestionAnswered.Count(q => q.Value == PeopleInGroup);
        }
    }
}