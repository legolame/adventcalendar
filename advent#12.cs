using System;
using System.Numerics;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    enum Direction
    {
        North, East, South, West
    }

    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            var ship = new Ship();

            var regex = new Regex(@"([A-Z])(\d+)");
            foreach (var line in lines)
            {
                var match = regex.Match(line);
                var amount = int.Parse(match.Groups[2].Value);
                switch (match.Groups[1].Value)
                {
                    case "N":
                        ship.MoveDirection(amount, Direction.North);
                        break;
                    case "S":
                        ship.MoveDirection(amount, Direction.South);
                        break;
                    case "E":
                        ship.MoveDirection(amount, Direction.East);
                        break;
                    case "W":
                        ship.MoveDirection(amount, Direction.West);
                        break;
                    case "L":
                        ship.Rotate(-amount);
                        break;
                    case "R":
                        ship.Rotate(amount);
                        break;
                    case "F":
                        ship.MoveFoward(amount);
                        break;
                }
            }

            Console.WriteLine(Math.Abs(ship.PositionX) + Math.Abs(ship.PositionY));


            ship = new Ship();
            foreach (var line in lines)
            {
                var match = regex.Match(line);
                var amount = int.Parse(match.Groups[2].Value);
                switch (match.Groups[1].Value)
                {
                    case "N":
                        ship.waypoint.MoveDirection(new Vector2(0, amount));
                        break;
                    case "S":
                        ship.waypoint.MoveDirection(new Vector2(0, -amount));
                        break;
                    case "E":
                        ship.waypoint.MoveDirection(new Vector2(amount, 0));
                        break;
                    case "W":
                        ship.waypoint.MoveDirection(new Vector2(-amount, 0));
                        break;
                    case "L":
                        ship.waypoint.Rotate(-amount);
                        break;
                    case "R":
                        ship.waypoint.Rotate(amount);
                        break;
                    case "F":
                        ship.MoveFowardWaypoint(amount);
                        break;
                }
            }

            Console.WriteLine(Math.Abs(ship.PositionX) + Math.Abs(ship.PositionY));

            Console.Read();
        }
    }

    class Ship
    {
        public int PositionX = 0;
        public int PositionY = 0;
        public Direction CurrentDirection = Direction.East;
        public Waypoint waypoint = new Waypoint();

        public void MoveFoward(int amount)
        {
            if(CurrentDirection == Direction.East)
            {
                PositionX += amount;
            }else if(CurrentDirection == Direction.West)
            {
                PositionX -= amount;
            }else if(CurrentDirection == Direction.North)
            {
                PositionY += amount;
            }
            else
            {
                PositionY -= amount;
            }
        }

        public void MoveDirection(int amount, Direction direction)
        {
            if (direction == Direction.East)
            {
                PositionX += amount;
            }
            else if (direction == Direction.West)
            {
                PositionX -= amount;
            }
            else if (direction == Direction.North)
            {
                PositionY += amount;
            }
            else
            {
                PositionY -= amount;
            }
        }

        public void Rotate(int amount)
        {
            amount = amount / 90;
            var currentIntDirection = (int)CurrentDirection;
            currentIntDirection += amount;
            if (currentIntDirection >= 4)
                currentIntDirection -= 4;
            if (currentIntDirection < 0)
                currentIntDirection += 4;

            CurrentDirection = (Direction)currentIntDirection;
        }

        public void MoveFowardWaypoint(int amount)
        {
            PositionX += amount * (int)waypoint.position.X;
            PositionY += amount * (int)waypoint.position.Y;
        }
    }

    class Waypoint
    {
        public Vector2 position = new Vector2(10, 1);


        public void MoveDirection(Vector2 vector)
        {
            position += vector;
        }

        public void Rotate(int amount)
        {
            var quaternion = Quaternion.CreateFromAxisAngle(new Vector3(0, 0, -1), (float) ((Math.PI / 180) * amount));
            position = Vector2.Transform(position, quaternion);

            position.X = (float) Math.Round(position.X);
            position.Y = (float)Math.Round(position.Y);
        }
    }
}
