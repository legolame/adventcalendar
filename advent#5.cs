using System;
using System.Collections.Generic;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            var existingIds = new List<int>();
            foreach (var line in lines)
            {
                existingIds.Add(GetTicketId(line));
            }

            existingIds.Sort();
            Console.WriteLine($"Max id is {existingIds[^1]}");

            for (int i = 0; i < existingIds.Count - 1; i++)
            {
                if (existingIds[i + 1] == existingIds[i] + 2)
                {
                    Console.WriteLine($"no seat occupied at seat id = {existingIds[i] + 1}");
                }
            }

            Console.Read();
        }

        static int GetTicketId(string boardingPass)
        {
            var binaryPass = boardingPass.Replace('F', '0').Replace('B', '1').Replace('R', '1').Replace('L', '0');
            return Convert.ToInt32(binaryPass, 2);
        }
    }
}