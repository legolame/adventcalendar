using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{

    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            var startingNumbers = lines[0].Split(',').Select(int.Parse).ToList();

            Dictionary<int, int> numberPositions = new Dictionary<int, int>();
            for (int i = 0; i < startingNumbers.Count; i++)
            {
                numberPositions[startingNumbers[i]] = i + 1;
            }

            var iterator = startingNumbers.Count;
            var currentNumber = 0; 
            while (true)
            {
                iterator++;

                var lastTurn = numberPositions.ContainsKey(currentNumber) ?  numberPositions[currentNumber] : 0;
                numberPositions[currentNumber] = iterator;

                if (iterator == 30000000)
                {
                    Console.WriteLine(currentNumber);
                    break;
                }

                if (lastTurn != 0)
                {
                    currentNumber = iterator - lastTurn;
                }
                else
                {
                    currentNumber = 0;
                }
            }



            Console.Read();
        }
    }
}
