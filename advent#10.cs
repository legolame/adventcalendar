using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            var joltAdapters = new List<int> { 0 };
            foreach (var line in lines)
            {
                joltAdapters.Add(int.Parse(line));
            }

            joltAdapters.Sort();
            joltAdapters.Add(joltAdapters[^1] + 3);

            var differenceOfOne = 0;
            var differenceOfThree = 0;
            var differenceOfTwo = 0;

            for (int i = 0; i < joltAdapters.Count - 1; i++)
            {
                var difference = joltAdapters[i + 1] - joltAdapters[i];

                differenceOfOne += difference == 1 ? 1 : 0;
                differenceOfThree += difference == 3 ? 1 : 0;
                differenceOfTwo += difference == 2 ? 1 : 0;
            }

            Console.WriteLine(differenceOfOne * differenceOfThree);
            Console.WriteLine(PartB(joltAdapters));

            Console.Read();
        }

        static double PartB(List<int> adapters)
        {

            //we only get permutable numbers
            var subList = new List<int>();
            for(int i = 1; i < adapters.Count -1; i++)
            {
                if (adapters[i + 1] - adapters[i] == 1 && adapters[i] - adapters[i - 1] == 1)
                {
                    subList.Add(adapters[i]);
                }
            }
            return ComputePermutations(subList, 0);
        }

        static double ComputePermutations(List<int> adapters, int index)
        {
            List<double> permutations = new List<double>();

            int streak = 1;
            for (int i = index; i < adapters.Count; i++)
            {
                if (i + 1 != adapters.Count && adapters[i + 1] == adapters[i] + 1)
                {
                    streak++;
                }
                else
                {
                    var minus = streak >= 3 ? GetTriangularNumber(streak - 2) : 0;
                    permutations.Add(Math.Pow(2, streak) - minus);
                    streak = 1;

                }
            }

            return permutations.Aggregate((a, x) => a * x);
        }

        static int GetTriangularNumber(int number)
        {
            return number * (number + 1) / 2;
        }
    }
}