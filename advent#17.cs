using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{

    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");
            Dictionary<(int,int,int, int), cube> cubes = new Dictionary<(int, int, int, int), cube>();

            var minX = 0;
            var maxX = lines[0].Length - 1;
            var minY = 0;
            var maxY = lines.Length - 1;
            var minZ = 0;
            var maxZ = 0;
            var maxW = 0;
            var minW = 0;

            for (int y = 0; y <= maxY; y++)
            {
                for(int x = 0; x <= maxX; x++)
                {
                    var cube = new cube((x, y, 0, 0));
                    if (lines[y][x] == '#')
                    {
                        cube._currentState = true;
                    }
                    cubes[(x, y, 0, 0)] = cube;
                }
            }

            for (int i = 0; i < 6; i++)
            {
                for (var x = minX - 1; x <= maxX + 1; x++)
                {
                    for (var y = minY - 1; y <= maxY + 1; y++)
                    {
                        for (var z = minZ - 1; z <= maxZ + 1; z++)
                        {
                            for (var w = minW - 1; w <= maxW + 1; w++)
                            {
                                if (!cubes.ContainsKey((x, y, z, w)))
                                    cubes[(x, y, z, w)] = new cube((x, y, z, w));
                            }
                        }
                    }
                }


                foreach (var cube in cubes)
                {
                    cube.Value.GetNextState4D(cubes);
                }

                foreach (var cube in cubes)
                {
                    cube.Value.SetNextState();
                }

                maxX++;
                minX--;
                maxY++;
                minY--;
                maxZ++;
                minZ--;
                maxW++;
                minW--;
            }

            var sum = cubes.Sum(c => c.Value._currentState ? 1 : 0);

            Console.Read();
        }
    }

    class cube
    {
        public bool _currentState;
        public bool _nextState;
        private (int x, int y, int z, int w) position;

        public cube((int, int, int, int) position)
        {
            this.position = position;
        }

        public void GetNextState(Dictionary<(int, int, int, int), cube> cubes)
        {
            var numberOfActiveNeighbors = 0;

            for (int x = position.x - 1; x <= position.x + 1; x++)
            {
                for (int y = position.y - 1; y <= position.y + 1; y++)
                {
                    for (int z = position.z - 1; z <= position.z + 1; z++)
                    {
                        if ((x, y, z, 0) == position)
                            continue;

                        if (!cubes.ContainsKey((x, y, z, 0)))
                            continue;

                        var neighbor = cubes[(x, y, z, 0)];
                        if (neighbor._currentState)
                            numberOfActiveNeighbors++;
                    }
                }
            }

            if (_currentState)
            {
                _nextState = numberOfActiveNeighbors == 2 || numberOfActiveNeighbors == 3;
            }
            else
            {
                _nextState = numberOfActiveNeighbors == 3;
            }
                
        }

        public void GetNextState4D(Dictionary<(int, int, int, int), cube> cubes)
        {
            var numberOfActiveNeighbors = 0;

            for (int x = position.x - 1; x <= position.x + 1; x++)
            {
                for (int y = position.y - 1; y <= position.y + 1; y++)
                {
                    for (int z = position.z - 1; z <= position.z + 1; z++)
                    {
                        for (int w = position.w - 1; w <= position.w + 1; w++)
                        {
                            if ((x, y, z, w) == position)
                                continue;

                            if (!cubes.ContainsKey((x, y, z, w)))
                                continue;

                            var neighbor = cubes[(x, y, z, w)];
                            if (neighbor._currentState)
                                numberOfActiveNeighbors++;
                        }
                    }
                }
            }

            if (_currentState)
            {
                _nextState = numberOfActiveNeighbors == 2 || numberOfActiveNeighbors == 3;
            }
            else
            {
                _nextState = numberOfActiveNeighbors == 3;
            }

        }

        public void SetNextState()
        {
            _currentState = _nextState;
            _nextState = false;
        }
    }
}
