using System;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            double slope1 = GetNumberOfTreeCollisionInSlope(lines, 1, 1);
            double slope2 = GetNumberOfTreeCollisionInSlope(lines, 3, 1);
            double slope3 = GetNumberOfTreeCollisionInSlope(lines, 5, 1);
            double slope4 = GetNumberOfTreeCollisionInSlope(lines, 7, 1);
            double slope5 = GetNumberOfTreeCollisionInSlope(lines, 1, 2);

            Console.WriteLine(slope1 * slope2 * slope3 * slope4 * slope5);
            Console.ReadLine();
        }

        static int GetNumberOfTreeCollisionInSlope(string[] lines, int horizontalJump, int verticalJump)
        {
            int lineSize = lines[0].Length;
            int horizontalPosition = 0;
            int verticalPosition = 0;
            int treeCounter = 0;

            while (verticalPosition < lines.Length)
            {
                if (horizontalPosition >= lineSize)
                    horizontalPosition -= lineSize;

                if (lines[verticalPosition][horizontalPosition] == '#')
                    treeCounter++;

                horizontalPosition += horizontalJump;
                verticalPosition += verticalJump;
            }

            return treeCounter;
        }
    }
}
