using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            Queue<uint> preamble = new Queue<uint>();

            int index = -1;
            uint wrongNumber = 0;
            foreach (var line in lines)
            {
                index++;
                uint number = uint.Parse(line);
                if (preamble.Count < 25)
                {
                    preamble.Enqueue(number);
                    continue;
                }

                if (IsNumberLegit(preamble, number))
                {
                    preamble.Dequeue();
                    preamble.Enqueue(number);
                }
                else
                {
                    wrongNumber = number;
                    Console.WriteLine();
                    break;
                }

            }

            Queue<uint> numberSet = new Queue<uint>();
            uint total;
            do
            {
                index--;
                numberSet.Enqueue(uint.Parse(lines[index]));

                total = (uint) numberSet.ToArray().Sum(n => n);

                if (total > wrongNumber)
                {
                    numberSet.Dequeue();
                }

            } while (total != wrongNumber);

            var setArray = numberSet.ToList();
            setArray.Sort();
            Console.WriteLine(setArray[0] + setArray[^1]);

            Console.Read();
        }

        static bool IsNumberLegit(Queue<uint> queue, uint numberToCheck)
        {
            var numberArray = queue.ToArray();

            for (int i = 0; i < numberArray.Length; i++)
            {
                for (int j = i + 1; j < numberArray.Length; j++)
                {
                    if (numberArray[i] + numberArray[j] == numberToCheck)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}