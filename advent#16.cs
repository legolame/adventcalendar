using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{

    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");
            var restrictionRegex = new Regex(@"([a-z\s]+):\s([0-9]+)-([0-9]+) or ([0-9]+)-([0-9]+)");
            var numberRegex = new Regex(@"[\d+,]+");

            List<Restriction> restrictions = new List<Restriction>();
            List<int> wrongNumbers = new List<int>();
            List<Ticket> validTickets = new List<Ticket>();

            foreach (var line in lines)
            {
                if (restrictionRegex.IsMatch(line))
                {
                    restrictions.Add(new Restriction(restrictionRegex.Match(line)));
                }
                else if (numberRegex.IsMatch(line))
                {
                    var numbers = line.Split(',').Select(c => int.Parse(c)).ToList();
                    var currentTicket = new Ticket(){numbers = numbers};
                    var ticketIsValid = true;
                    foreach (var number in numbers)
                    {
                        bool numberIsValid = false;
                        foreach (var restriction in restrictions)
                        {
                            if (restriction.NumberIsInBound(number))
                            {
                                numberIsValid = true;
                                break;
                            }
                        }

                        if (!numberIsValid)
                        {
                            wrongNumbers.Add(number);
                            ticketIsValid = false;
                        }
                    }

                    if(ticketIsValid)
                        validTickets.Add(currentTicket);
                }
            }

            var sum = wrongNumbers.Sum(n => n);


            Dictionary<int, List<string>> positionRestrictions = new Dictionary<int, List<string>>();

            //we get the first possible restriction
            for (var i = 0; i < validTickets[1].numbers.Count; i++)
            {
                positionRestrictions[i] = new List<string>();

                foreach (var restriction in restrictions)
                {
                    if (restriction.NumberIsInBound(validTickets[1].numbers[i]))
                    {
                        positionRestrictions[i].Add(restriction.name);
                    }
                }
            }

            //we eliminate invalid restrictions
            foreach (var validTicket in validTickets)
            {
                for (var i = 0; i < validTicket.numbers.Count; i++)
                {

                    foreach (var restriction in restrictions)
                    {
                        if (!restriction.NumberIsInBound(validTicket.numbers[i]))
                        {
                            positionRestrictions[i].Remove(restriction.name);
                        }
                    }
                }
            }

            //we prevent single restriction to be repeated
            var onlyOneRestEverywhere = false;
            while (!onlyOneRestEverywhere)
            {
                onlyOneRestEverywhere = true;
                foreach (var pr in positionRestrictions)
                {
                    if (pr.Value.Count == 1)
                    {
                        foreach (var prToAdapt in positionRestrictions)
                        {
                            if (pr.Key != prToAdapt.Key)
                            {
                                prToAdapt.Value.Remove(pr.Value[0]);
                            }
                        }
                    }
                    else
                    {
                        onlyOneRestEverywhere = false;
                    }
                }
            }

            long total = 1;
            foreach (var pr in positionRestrictions)
            {
                if (pr.Value[0].Contains("departure"))
                {
                    total *= validTickets[0].numbers[pr.Key];
                }
            }

            Console.Read();
        }
    }

    class Restriction
    {
        public string name;
        private (int min, int max) _firstRestriction;
        private (int min, int max) _secondRestriction;
        private int order;

        public Restriction(Match match)
        {
            name = match.Groups[1].Value;

            _firstRestriction.min = int.Parse(match.Groups[2].Value);
            _firstRestriction.max = int.Parse(match.Groups[3].Value);

            _secondRestriction.min = int.Parse(match.Groups[4].Value);
            _secondRestriction.max = int.Parse(match.Groups[5].Value);
        }

        public bool NumberIsInBound(int number)
        {
            return (number >= _firstRestriction.min && number <= _firstRestriction.max) ||
                   (number >= _secondRestriction.min && number <= _secondRestriction.max);
        }
    }

    class Ticket
    {
        public List<int> numbers = new List<int>();
    }
}
