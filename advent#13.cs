using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventCalendar
{

    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "/Input.txt");

            var timeStamp = int.Parse(lines[0]);
            var numberRegex = new Regex(@"\d+");
            var matches = numberRegex.Matches(lines[1]);
            (int time, int id) minTime = (int.MaxValue, 0);
            foreach (Match match in matches)
            {
                var id = int.Parse(match.Value);
                var nearestCoef = timeStamp / id;
                
                if ((nearestCoef + 1) * id < minTime.time)
                {
                    minTime = ((nearestCoef + 1) * id, id);
                }
            }

            Console.WriteLine(minTime.id * (minTime.time - timeStamp));

            //Part B

            var positionInLine = lines[1].Split(',').ToList();
            List<Bus> buses = new List<Bus>();
            foreach (Match match in matches)
            {
                var bus = new Bus();
                bus.Id = int.Parse(match.Value);
                bus.PositionInLine = positionInLine.IndexOf(bus.Id.ToString());
                buses.Add(bus);
            }

            double offset = 0;
            double increment = buses[0].Id;
            for (int i = 1; i < buses.Count; i++)
            {
                var subList = buses.GetRange(1, i);
                var result = ComputeBusRecuringTimeStampInfos(subList, offset, increment);
                offset = result.offSet;
                increment = result.increment;
            }

            Console.WriteLine(offset);
            Console.Read();
        }

        static (double offSet, double increment) ComputeBusRecuringTimeStampInfos(List<Bus> buses, double offset, double increment)
        {
            (double offSet, double increment) result = (0, 0);
            var x = offset;
            while (true)
            {
                var ok = true;
                for (int i = 0; i < buses.Count; i++)
                {
                    if ((x + buses[i].PositionInLine) % buses[i].Id != 0)
                    {
                        ok = false;
                    }
                }

                if (ok && result.offSet == 0)
                {
                    result.offSet = x;
                }else if (ok && result.offSet != 0)
                {
                    result.increment = x - result.offSet;
                    break;
                }

                x += increment;
            }

            return result;
        } 
    }

    class Bus
    {
        public int Id;
        public int PositionInLine;
    }
}
